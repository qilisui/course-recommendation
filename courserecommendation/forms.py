from django import forms
from django.contrib.auth.models import User
import json,re
from django.utils.encoding import smart_str

class StudentNameForm(forms.Form):
    student_name = forms.CharField(label='Student Name', required=True)

    def clean(self):
        cleaned_data = super(StudentNameForm, self).clean()
        return cleaned_data