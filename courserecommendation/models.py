from django.db import models

# Create your models here.
from neomodel import StructuredNode, StringProperty, IntegerProperty,UniqueIdProperty, RelationshipTo

class Site(StructuredNode):
    id = StringProperty(unique_index=True, required=True)
    site = StringProperty(index=True, default="site")

class Course(StructuredNode):
    id = UniqueIdProperty()
    title = StringProperty(unique_index=True)
    courseId = IntegerProperty(index=True, default=0)
    maxGrade = IntegerProperty(index=True, default=0)
    minGrade = IntegerProperty(index=True, default=0)
    subjectArea = IntegerProperty(index=True, default=0)
    tierLevelId = IntegerProperty(index=True, default=0)

    # Relations :
    site = RelationshipTo(Site, 'HAPPENSAT')