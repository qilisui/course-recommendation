from django.urls import path
from courserecommendation import views

urlpatterns = [
    path("", views.home, name="home"),
    # path("query-result", views.query_result, name="query-result"),
]