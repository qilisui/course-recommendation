from django.shortcuts import render
from courserecommendation import neo4j_connection
from courserecommendation.forms import StudentNameForm
from neo4j import __version__ as neo4j_version
from .neo4j_connection import Neo4jConnection
from django.http import HttpResponseRedirect
from neo4j import GraphDatabase,Session

# Create your views here.
def home(request):
    conn = Neo4jConnection(uri="bolt://localhost:7687", user="qse6346", pwd="suiqiliqi")
    form = StudentNameForm()
    fixed_query_1_result = None
    attended_by_classmates_lst = []
    same_program_course_lst = []
    teacher_other_course_lst = []
    site_other_course_lst = []
    show_course_list = False
    student_name = ''

    if request.method == 'POST':
        show_course_list = True
        form = StudentNameForm(request.POST)
        if form.is_valid():
            student_name = form.cleaned_data['student_name']
            # fixed query 1 : A list of courses which attended by random classmates
            attended_by_classmates = 'match (wu:Student{name:"%s"})-[:ATTEND]->(c)<-[:ATTEND]-(classmate)-[:ATTEND]->(c2) WHERE NOT (wu)-[:ATTEND]->(c2) RETURN c2.title AS courseTitle'%student_name
            attended_by_classmates_result = conn.query(attended_by_classmates,db='neo4j')
            for i in attended_by_classmates_result:
                if i["courseTitle"] not in attended_by_classmates_lst:
                    attended_by_classmates_lst.append(i["courseTitle"])

            # attended_by_classmates_graph = 'match r = (wu:Student{name:"%s"})-[:ATTEND]->(c)<-[:ATTEND]-(classmate)-[:ATTEND]->(c2) RETURN *'%student_name
            # fixed_query_1_result_graph = conn.query(attended_by_classmates_graph,db='neo4j')
            # print(fixed_query_1_result_graph)
            # print(type(fixed_query_1_result_graph[0]))

            # for i in fixed_query_1_result_graph:
            #     # print(i)
            #     pass

            # fixed query 2 : 2. Courses belongs to the same program that user did not attend
            same_program_course = 'match (wu:Student{name:"%s"})-[:ATTEND]->(c)-[:BELONGSTO]->(program)<-[:BELONGSTO]-(c2) WHERE NOT (wu)-[:ATTEND]->(c2) RETURN c2.title AS courseTitle'%student_name
            same_program_course_result = conn.query(same_program_course,db='neo4j')
            for i in same_program_course_result:
                if i["courseTitle"] not in same_program_course_lst:
                    same_program_course_lst.append(i["courseTitle"])

            # fixed query 3 :3. Courses that user’s teacher taught but user did not attend
            teacher_other_course = 'match (wu:Student{name:"%s"})-[:ATTEND]->(c)<-[:TEACHES]-(teacher)-[:TEACHES]->(c2) WHERE NOT (wu)-[:ATTEND]->(c2) RETURN c2.title AS courseTitle'%student_name
            teacher_other_course_result = conn.query(teacher_other_course,db='neo4j')
            for i in teacher_other_course_result:
                if i["courseTitle"] not in teacher_other_course_lst:
                    teacher_other_course_lst.append(i["courseTitle"])

            # fixed query 6 :6. Course happens at the same site that user did not attend
            site_other_course = 'match (wu:Student{name:"%s"})-[:ATTEND]->(c)-[:HAPPENSAT]->(Site)<-[:HAPPENSAT]-(c2) WHERE NOT (wu)-[:ATTEND]->(c2) RETURN c2.title AS courseTitle'%student_name
            site_other_course_result = conn.query(site_other_course,db='neo4j')
            for i in site_other_course_result:
                if i["courseTitle"] not in site_other_course_lst:
                    site_other_course_lst.append(i["courseTitle"])

            # TODO:
            # Try generate graph from query result
    else:
        form = StudentNameForm()
    return render(
        request,'home.html',
        {
            'form':form,
            'attended_by_classmates_lst':attended_by_classmates_lst,
            'same_program_course_lst':same_program_course_lst,
            'teacher_other_course_lst':teacher_other_course_lst,
            'site_other_course_lst':site_other_course_lst,
            'show_course_list': show_course_list,
            'student_name':student_name,
        })